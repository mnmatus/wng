﻿wngapp.controller('indexController', function ($scope, $http, $q, $window, $location) {    
    $scope.activeIndex;
    $scope.listCard = [
          { url: '/images/redimage.jpg', isActive: false, isCat: false }
        , { url: '/images/redimage.jpg', isActive: false, isCat: false }
        , { url: '/images/redimage.jpg', isActive: false, isCat: false }
        , { url: '/images/redimage.jpg', isActive: false, isCat: false }
    ];
   
    $scope.setStatus = function (index) {        
        angular.forEach($scope.listCard, function (value, key) {
            if (!$scope.listCard[key].isCat)
            {
                $scope.listCard[key].url = "/images/redimage.jpg";
                $scope.listCard[key].isActive = false;
            }
        });
        if ($scope.activeIndex != index) {
            $scope.listCard[index].url = '/images/greenimage.jpg';
            $scope.listCard[index].isActive = true;
            $scope.listCard[index].isCat = false;
            $scope.activeIndex = index;
        }
        else {
            $scope.activeIndex = null;
        };
        
    };
    
    $scope.getImages = function () {
        var numberOfImage = $scope.listCard.length;       
        $http({
            method: 'GET',            
            url: '/api/main/getimages/' + numberOfImage,
        }).then(function (response) {            
            for (var i = 0; i < numberOfImage; i++) {                
                $scope.listCard[i].url = response.data[i];
                $scope.listCard[i].isCat = true;
            }            
        }).catch(function (response) {            
            alert('Unexpected error occured');
        });        
    };

    $scope.addCard = function () {
        var newCard = { url: '/images/redimage.jpg', isActive: false, isCat: false }
        $scope.listCard.push(newCard);
    };    
});